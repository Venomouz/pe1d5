import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { NavController, ToastController } from '@ionic/angular';
import firebase from 'firebase/compat';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  isLoggedIn: Observable<firebase.User>;

  constructor(
    private nav: NavController,
    private auth: AngularFireAuth,
    private toast: ToastController
  ) {
    this.isLoggedIn = this.auth.authState;
  }

  login(user: { email: string; password: string }) {
    this.auth.signInWithEmailAndPassword(user.email, user.password).
    then(() => this.nav.navigateForward('home')).
    catch (() => this.showError());
  }

  logout() {
    this.auth.signOut().
    then(() => this.nav.navigateBack('login'));
  }

  createUser(user: { email: string; password: string }) {
    this.auth.createUserWithEmailAndPassword(user.email, user.password).
    then(credentials => console.log(credentials));
  }

  recoverPass(data: { email: string }) {
    this.auth.sendPasswordResetEmail(data.email).
    then(() => this.nav.navigateBack('login')).
    catch(err => {
      console.log(err);
    });
  }

  private async showError() {
    const ctrl = await this.toast.create({
      message: 'Dados de acesso incorretos',
      duration: 3000
    });
    ctrl.present();
  }
}